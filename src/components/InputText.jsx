import React from 'react';

export default React.createClass({
  getInitialState: function() {
    return {
      value: this.props.value || ''
    };
  },
  onChange: function(e) {
    this.setState({
      value: e.target.value
    });
    //if (this.props.onChange)
    //  this.props.onChange
  },
  render: function() {
    return <input type="text" value={this.state.value} onChange={this.onChange} />;
  }
});