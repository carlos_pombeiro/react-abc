import React from 'react';
import InputText from './InputText';

export default React.createClass({
  getInitialState: function() {
    return this.props.model || {
      name: 'world'
    };
  },
  onChange: function(e) {
    this.setState({
      name: e.target.value
    });
  },
  render: function() {
    return <form>
      <InputText value={this.state.name}></InputText>
      <input type="text" value={this.state.name} onChange={this.onChange} />
      name: {this.state.name}
    </form>;
  }
});