//
// # SimpleServer
//
// A simple chat server using Socket.IO, Express, and Async.
//
var http = require('http');
var path = require('path');

var async = require('async');
//var socketio = require('socket.io');
var express = require('express');

var router = express();
var server = http.createServer(router);

//router.use(express.static(path.resolve(__dirname, 'client')));
router.use(express.static(path.resolve(__dirname, '')));

server.listen(/*process.env.PORT || */8080, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Listening at", addr.address + ":" + addr.port);
});
